---
title: "Horaires"
weight: 2
header_menu: true
---

---

### {{<icon class="fa fa-recycle">}} Le dépôt
- Du lundi au vendredi de 8h à 12h et de 13h30 à 16h30.
- Le samedi de 09h à 12h et de 14h à 16h30.

### {{<icon class="fa fa-shopping-cart">}} La boutique

- Du lundi au vendredi de 10h à 12h et de 13h30 à 16h30.
- Le samedi de 09h à 12h et de 14h à 17h.

###  {{<icon class="fa fa-truck">}} La collecte des encombrants
Sur rendez-vous au *04 94 80 47 45* du lundi au vendredi de 8h à 12h et de 13h à 16h.

