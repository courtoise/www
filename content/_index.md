---
header_image: "images/cover.jpg"
header_logo: "images/logo_simple.png"
header_headline: "La Courtoise Ressourcerie"
header_subheadline: "La Courtoise Ressourcerie est un chantier d’insertion qui valorise les objets en leur donnant une seconde vie tout en créant de l’emploi et en agissant pour l’environnement."
---
